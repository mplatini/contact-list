//
//  ContactViewController.swift
//  contact_list
//
//  Created by The App Experts on 06/03/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit


class ContactTableViewCell: UITableViewCell{

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelNumber: UILabel!
    
    var model: ContactModel!
    weak var delegate : ContactTableViewCellDelegate?
    @IBOutlet weak var callButtonOutlet: UIButton!
    
    override func awakeFromNib() {
      super.awakeFromNib()
      self.callButtonOutlet.addTarget(self, action: #selector(callActionButton(_:)), for: .touchUpInside)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
      super.setSelected(selected, animated: animated)

    }
    
    @IBAction func callActionButton(_ sender: Any) {

        if let model = model,
              let _ = delegate {
            self.delegate?.contactTableViewCell(self, contactButtonTappedFor: model)
        }
    }
}

protocol ContactTableViewCellDelegate: AnyObject {
  func contactTableViewCell(_ contactTableViewCell: ContactTableViewCell, contactButtonTappedFor model: ContactModel)
}


class ContactViewController: UIViewController {

    var model: ContactModel!
    
    @IBOutlet weak var textFieldName: UITextField!
    @IBOutlet weak var textFieldNumber: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    
    @IBAction func buttonInsert(_ sender: UIButton) {
        
        guard let name = textFieldName.text else{
            return
        }
        
        //ALERT - NAME IS MISSING
        if name.isEmpty {
            showAlertInvalidName()
        }
        
        guard let number = textFieldNumber.text else{
            return
        }
        
        let newContact = Contact(fullName: name, contactNumber: number)
        let arrayNewContact = [newContact]
        
        guard let firstCharacter = newContact.fullName.first else{
            return
        }
        
        model.addContact(newKey: String(firstCharacter), newValues: arrayNewContact)
        
        textFieldName.text = nil
        textFieldNumber.text = nil
        
        tableView.reloadData()
        
        
    }
    
    
    
    @IBAction func buttonCall(_ sender: Any) {
        guard let numberIsEmpty = textFieldNumber.text else{
            return
        }
        if numberIsEmpty.isEmpty {
           showAlerNoPhoneNumber()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        model = ContactModel()
        
        
        tableView.estimatedRowHeight = 200.0 // Adjust Primary table height
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    
    func showAlertInvalidName() {
        
       let alertController = UIAlertController(title: "Name Invalid!", message: "Insert a valid name", preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default)
        alertController.addAction(OKAction)
        
        present(alertController, animated: true, completion: nil)
        
    }
    
    
    func showAlerNoPhoneNumber() {
        
       let alertController = UIAlertController(title: "No Phone Number", message: "This Contact There is NO Phone Number", preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default)
        alertController.addAction(OKAction)
        
        present(alertController, animated: true, completion: nil)
        
    }
    
    
}//end_class...


extension ContactViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return model.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.numberOfRow(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "contact_cell", for: indexPath) as! ContactTableViewCell
        
        guard let item = model.item(at: indexPath) else { return cell }
        
        cell.labelName.text = item.fullName
        cell.labelNumber.text = item.contactNumber
        
        cell.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return model.sections[section]
    }
}

extension ContactViewController: ContactTableViewCellDelegate{
    
    func contactTableViewCell(_ contactTableViewCell: ContactTableViewCell, contactButtonTappedFor model: ContactModel) {
      // directly use the contact saved in the cell
      // show alert
        var alert = UIAlertController(title: "There IS Number", message: "There is number", preferredStyle: .alert)
        

        if let result = contactTableViewCell.labelNumber.text{
            if result.isEmpty{

                alert = UIAlertController(title: "No Number", message: "There is NO number", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert.addAction(okAction)
            }

        }
        
        
        
        
      
      self.present(alert, animated: true, completion: nil)
    }
}



extension ContactViewController: UITableViewDelegate {}
