//
//  ContactModel.swift
//  contact_list
//
//  Created by The App Experts on 06/03/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import Foundation


struct Contact {
    var fullName: String
    var contactNumber: String
}

class ContactModel{
    
    //A - [A_NAME1, A_NAME2, A_NAME3...A_NAMEN-1]
    //B - [B_NAME1, B_NAME2, B_NAME3...B_NAMEN-1]
    //...
    //Z - [Z_NAME1, Z_NAME2, Z_NAME3...Z_NAMEN-1]
    public var contacts: [String : [Contact]]
    
    init() {
        //contacts = ["M": [Contact(fullName: "Mychel", contactNumber: "999")  ]]
        contacts = [ : ]
    }

    func addContact(newKey: String, newValues: [Contact]){
        
        for key in Array(contacts.keys){
            
            //print("key (dic) \(key.uppercased()) - new key \(newKey.uppercased()) ")
            
            if (key.uppercased() == newKey.uppercased()){
                guard var currentArray = contacts[key] else{
                    return
                }
                
                let contato = newValues[0]
                currentArray.append(contato)
                
                print(" (before) contacts - \(contacts)" )
                contacts.updateValue(currentArray, forKey: newKey.uppercased())
                print(" (after) contacts - \(contacts)" )
            }
        }
        
        contacts.updateValue(newValues, forKey: newKey.uppercased())
    }
}//end_class...



//Helper methods...
extension ContactModel{

    var sections: [String] {
        return contacts.keys.sorted()
    }
    
    
    
    func numberOfRow(in section: Int) -> Int {
        
        guard section < sections.count else { return 0 }
        
        let key = sections[section]
        
        guard let values = contacts[key] else { return 0 }
        
        return values.count
    }
    
    
    
    func item(at indexPath: IndexPath) -> Contact? {

        guard indexPath.section < sections.count else { return nil }

        let key = sections[indexPath.section]

        guard let values = contacts[key], indexPath.row < values.count else { return nil }

        return values[indexPath.row]

    }
}
